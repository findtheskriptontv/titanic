import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    Mr = df[df['Name'].str.contains(r'Mr\.', regex=True)]
    x = Mr['Age'].isna().sum()
    y = int(Mr['Age'].median().round())

    Mrs = df[df['Name'].str.contains(r'Mrs\.', regex=True)]
    k = Mrs['Age'].isna().sum()
    m = int(Mrs['Age'].median().round())

    Miss = df[df['Name'].str.contains(r'Miss', regex=True)]
    l = Miss['Age'].isna().sum()
    n = int(Miss['Age'].median().round())
    

    return [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
